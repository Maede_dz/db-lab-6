import { Body, Controller, Get, ParseIntPipe, Post, Put, Param, Delete } from '@nestjs/common';
import CreateBookDto from '../dto/create-book.dto';
import { BooksService } from './books.service';
import { identity } from 'rxjs';

@Controller('books')
export default class BooksController {
  constructor(private readonly booksServiece: BooksService) {}
  @Post('post')
  postBook(@Body() book: CreateBookDto) {
    return this.booksServiece.insert(book);
  }
  @Get()
  getAll() {
    return this.booksServiece.getAllBooks();
  }
  

  @Put(':id/update')
  async update(@Param('id') id:string, @Body() createBookDto:CreateBookDto):Promise<any>{
    return this.booksServiece.putBook(Number(id), createBookDto);
  }

  @Delete(':id/delete')
  async delete(@Param('id') id:string):Promise<any>{
    return this.booksServiece.deleteBook(id);
  }
}
