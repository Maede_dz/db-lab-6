export default class CreateTodoDto {
    readonly text: string;
    readonly category: string;
    readonly labelIds: [];
    readonly todoId: [];
}
