export default class CreateJobDto {
    readonly type: string;
    readonly appId: number;
}
