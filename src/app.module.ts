import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import UserEntity from './db/user.entity';
import BooksModule from './books/books.module';
import GenreModule from './genre/genre.module';
import BookEntity from './db/book.entity';
import GenreEntity from './db/genre.entity';
import * as path from 'path';
import { TodoController } from './todo/todo.controller';
import { TodoService } from './todo/todo.service';
import { TodoModule } from './todo/todo.module';
import { LabelController } from './label/label.controller';
import { LabelService } from './label/label.service';
import { LabelModule } from './label/label.module';
import { JobController } from './job/job.controller';
import { JobService } from './job/job.service';
import { JobModule } from './job/job.module';


@Module({
  imports: [
    TodoModule,
    LabelModule,
    JobModule,
    // TypeOrmModule.forFeature([TodoModule, LabelModule, JobModule]),

    TypeOrmModule.forRoot(),

    AuthModule,

    UsersModule
    
  ],
  controllers: [AppController, TodoController, LabelController, JobController],
  providers: [AppService, TodoService, LabelService, JobService],
})
export class AppModule {}
