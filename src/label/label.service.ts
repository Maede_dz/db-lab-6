import { Injectable } from '@nestjs/common';
import CreateLabelDto from 'src/dto/create-label.dto';
import LabelEntity from 'src/db/label.entity';

@Injectable()
export class LabelService {
    async insert(labelTitle:CreateLabelDto):Promise<LabelEntity>{
        const labelEntity: LabelEntity = LabelEntity.create();
        const {type} = labelTitle;

        labelEntity.type = type;
        LabelEntity.save(labelEntity);
        return labelEntity;
    }

    async getAllLabels():Promise<LabelEntity[]>{
        return await LabelEntity.find();
    }
}
