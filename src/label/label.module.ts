import { Module } from '@nestjs/common';
import { LabelController } from './label.controller';
import { LabelService } from './label.service';

@Module({
    imports: [],
    controllers: [LabelController],
    providers: [LabelService],
})
export class LabelModule {}