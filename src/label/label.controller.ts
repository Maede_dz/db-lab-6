import { Controller, Post, Body, Get } from '@nestjs/common';
import { LabelService } from './label.service';
import CreateLabelDto from 'src/dto/create-label.dto';

@Controller('label')
export class LabelController {
    constructor(private readonly labelService:LabelService){}
    @Post('post')
    postLabel(@Body() label:CreateLabelDto){
        return this.labelService.insert(label);
    }
    @Get()
    getAll(){
        return this.labelService.getAllLabels();
    }
}
