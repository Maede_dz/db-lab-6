import { Controller, Post, Body, Get, Put, Param, Delete } from '@nestjs/common';
import { TodoService } from './todo.service';
import CreateTodoDto from 'src/dto/create-todo.dto';

@Controller('todo')
export class TodoController {
    constructor(private readonly todoServices:TodoService){}

    @Post('post')
    postTodo(@Body() todo: CreateTodoDto){
        return this.todoServices.insert(todo);
    }
    @Get()
    getAll(){
        return this.todoServices.getAllApps();
    }

    @Put(':id/update')
    async update(@Param('id') id: string, @Body() createTodoDto: CreateTodoDto): Promise<any> {
        return this.todoServices.putApp(Number(id), createTodoDto);
    }

    @Delete(':id/delete')
    async delete(@Param('id') id:string):Promise<any>{
        return this.todoServices.deleteApp(id);
    }

}
