import { Injectable } from '@nestjs/common';
import CreateTodoDto from 'src/dto/create-todo.dto';
import TodoEntity from 'src/db/todo.entity';
import LabelEntity from 'src/db/label.entity';
import JobEntity from 'src/db/job.entity';

@Injectable()
export class TodoService {
    async insert(todo:CreateTodoDto):Promise<TodoEntity>{
        const todoEntity: TodoEntity = TodoEntity.create();
        const { text, category, labelIds, todoId} = todo;

        todoEntity.category = category;
        todoEntity.jobs = [];
        todoEntity.labels = [];
        todoEntity.text = text;

        for (let i = 0; i < labelIds.length; i++) {
            const l = await LabelEntity.findOne(labelIds[i]);
            todoEntity.labels.push(l);
        }

        for (let i = 0; i < todoId.length; i++) {
            const l = await JobEntity.findOne(todoId[i]);
            todoEntity.jobs.push(l);
        }
        TodoEntity.save(todoEntity);
        return todoEntity;
    }

    async getAllApps():Promise<TodoEntity[]>{
        return await TodoEntity.find();
    }

    async deleteApp(id) {
        return await TodoEntity.delete(id);
    }

    async putApp(id: number, appDetail: CreateTodoDto): Promise<TodoEntity> {
        const { text, category, labelIds, todoId } = appDetail;
        const app: TodoEntity = new TodoEntity();
        app.category = category;
        app.labels = [];
        app.jobs = [];
        app.id = id;
        app.text = text;

        for (let i = 0; i < labelIds.length; i++) {
            const l = await LabelEntity.findOne(labelIds[i]);
            app.labels.push(l);
        }

        for (let i = 0; i < todoId.length; i++) {
            const l = await JobEntity.findOne(todoId[i]);
            app.jobs.push(l);
        }
        TodoEntity.merge(app);
        return app;
    }
}
