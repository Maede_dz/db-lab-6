import { Injectable } from '@nestjs/common';
import CreateJobDto from 'src/dto/create-job.dto';
import JobEntity from 'src/db/job.entity';
import TodoEntity from 'src/db/todo.entity';

@Injectable()
export class JobService {
    async insert(jobDetail: CreateJobDto): Promise<JobEntity> {
        const jobEntity: JobEntity = JobEntity.create();
        const { type, appId } = jobDetail;

        jobEntity.type = type;
        jobEntity.app = await TodoEntity.findOne(appId);;
        await JobEntity.save(jobEntity);
        return jobEntity;
    }

    async getAllJobs(): Promise<JobEntity[]> {
        return await JobEntity.find();
    }
}
