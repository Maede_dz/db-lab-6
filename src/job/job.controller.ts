import { Controller, Post, Body, Get } from '@nestjs/common';
import { JobService } from './job.service';
import CreateJobDto from 'src/dto/create-job.dto';

@Controller('job')
export class JobController {
    constructor(private readonly jobServices:JobService){}
    @Post('post')
    postJob(@Body() job:CreateJobDto){
        return this.jobServices.insert(job);
    }
    @Get()
    getAll(){
        return this.jobServices.getAllJobs();
    }
}
