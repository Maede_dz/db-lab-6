import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    ManyToMany,
    ManyToOne,

} from 'typeorm';
import TodoEntity from './todo.entity';


@Entity()
export default class JobEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: string;

    @ManyToOne((type) => TodoEntity, (todo) => todo.jobs)
    app: TodoEntity;
}

