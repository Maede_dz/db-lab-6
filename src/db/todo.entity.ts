import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BaseEntity,
    ManyToOne,
    ManyToMany,
    JoinTable,
    OneToMany,
} from 'typeorm';

import LabelEntity from './label.entity';
import JobEntity from './job.entity';


@Entity()
export default class TodoEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 500 })
    text: string;

    @Column({length:200})
    category: String;

    @ManyToMany((type) => LabelEntity)
    @JoinTable()
    labels: LabelEntity[];

    @OneToMany((type) => JobEntity, (job) => job.app)
    jobs: JobEntity[];

}
